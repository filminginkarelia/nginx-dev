FROM nginx:stable-alpine

COPY default.conf /etc/nginx/conf.d/default.conf

RUN apk add --update openssl bash && \
    rm -rf /var/cache/apk/* && \
    rm -rf /tmp/*

EXPOSE 80

WORKDIR /etc/nginx


